# README #


## 1. Introdução ##

Este repositório contém o código fonte do componente **bancodemassa-ws-node** da solução **Bancodemassa - Cantina**. O componente *bancodemassa-ws-node* é um pacote .war que disponibiliza serviços através de WebService a solução de *Bancodemassa Cantina*.

### 2. Documentação ###

### 2.1. Diagrama de Caso de Uso (Use Case Diagram) ###

```image-file
./doc/UseCaseDiagram*.jpg
../../bancodemassa-doc/*
```

### 2.2. Diagrama de Implantação (Deploy Diagram) ###

```image-file
./doc/DeployDiagram*.jpg
../../bancodemassa-doc/*
```

### 2.3. Diagrama Modelo Banco de Dados (Database Data Model) ###

* n/a

## 3. Projeto ##

### 3.1. Pré-requisitos ###

* Linguagem de programação: Java
* IDE: Eclipse
* Apache Tomcat v8.0 para Eclipse
* JDK/JRE: 1.7 ou 1.8
* Postgresql 9.5+
* Postgresql database schemas ('bmnode') instalado
* Postgresql database initialization/configuration scripts executados

### 3.2. Guia para Desenvolvimento ###

* Obtenha o código fonte através de um "git clone". Utilize a branch "master" se a branch "develop" não estiver disponível.
* Faça suas alterações, commit e push na branch "develop".


### 3.3. Guia para Configuração ###

Os principais itens da gestão de configuração do 'bancodemassa-ws-node' são:

a. Em './src/main/resources/application.properties' configure o host, porta, dbname, username e password do banco de dados Postgresql que armazena as configurações de nos de servicos do banco de massa

```properties
spring.datasource.url=jdbc:postgresql://localhost:5432/bmnode
spring.datasource.username=postgres
spring.datasource.password=postgres
spring.datasource.driver-class-name=org.postgresql.Driver
spring.datasource.platform=postgres
```


### 3.4. Guia para Teste ###

a. CT-01-01: Teste técnico do componente stored function do 'bancodemassa-scripts' está respondendo as requisições que serão feitas pelo componente WebService 'bancodemassa-ws-node'. 

Este componente  responde a uma requisição de solicitação de massa. O parâmetro de entrada é um Json com especificação de ( ClasseMassa, ContentType, EnvironmentType ). Para simplificar vamos passar um Json vazio e a resposta esperada deve ser um erro de não reconhecimento dos elementos, mas valida que o componente está funcionando.

```sql
\CONNECT bmnode
SELECT fx_node_test_data_get('{}');
-----------------------------------
{"get_return": {"id": null, "md5": null, "data": null, "status_code": 0, "status_message": "ERROR: Unrecognized classe_massa: "}}
```


b. Na tabela 'bm_node_instancia_massa' do do banco de dados 'bmnode' Postgresql deverá existir uma lista de massa de teste disponível. A coluna 'classe_massa' categoriza as massas dando opções de substituições. A coluna 'status' informa se esta massa está disponível para uso e se já foi utilizada alguma vez. A coluna json_massa comtém as informações da massa de teste, que neste caso são usuários e logins de sistemas. Todos estes registros foram inseridos pelo componente 'bancodemassa-scripts' durante a execução do processo de ETL-Extração Transformação e Carga das massas.
- SELECT
```sql
SELECT im.id,
       cm.classe_massa,
	   sm.status_massa,
	   im.json_massa
FROM   bm_node_instancia_massa im
INNER  JOIN bm_node_classe_massa cm
ON     cm.id = im.node_classe_massa_id
INNER  JOIN bm_node_status_massa sm
ON     sm.id = im.node_status_massa_id
LIMIT  10
;
```
- RESULT-SET
```sql
id  | classe_massa |      status_massa      | json_massa                
----+--------------+------------------------+---------------------------------------------------------------------------------------------------
  1 | ETL.LOGIN    | Disponivel para Uso   | {"tags": {"cod_reserva": null, "classe_massa": "ETL.LOGIN"}, "massa": {"sistema": "FACEBOOK", "login_name": "josemarsilva", "login_password": "Secret@123"}}
  2 | ETL.LOGIN    | Disponivel para Reuso | {"tags": {"cod_reserva": null, "classe_massa": "ETL.LOGIN"}, "massa": {"sistema": "CRMPRO", "login_name": "josemarsilva", "login_password": "secret123"}}
```


c. CT-01-02: Teste basico verificar se o WebService 'bancodemassa-ws-node' está funcionando

Suba o servico 'bancodemassa-ws-node' ( Eclipse Run >> Run As >> Tomcat v.8 Server ) e verifique se ele está recebendo as solicitações. A resposta esperada para um Json vazio '{}' é que a classe de massa da solicitação não foi reconhecida.

- request:
```browser
http://localhost:8080/bancodemassa-ws-node/dados/{} 
```

- response:

```json
{"get_return": {"id": null, "md5": null, "data": null, "status_code": 0, "status_message": "ERROR: Unrecognized classe_massa: "}}
```


d. CT-01-04: Teste para obter a massa de uma requisição com os seguintes parametros: i) ambiente EnvironmentType='HML'; ii) tipo de conteúdo ContentType='ETL'; iii) classe de massa 'ETL.LOGIN'

- request:
```browser
http://localhost:8080/bancodemassa-ws-node/dados/{"get":{"classe_massa":"ETL.LOGIN"}}
```

- response:

```json
{"get_return": {"id": 1, "md5": "c4ca4238a0b923820dcc509a6f75849b", "data": {"tags": {"cod_reserva": null, "classe_massa": "ETL.LOGIN"}, "massa": {"obs": "", "sistema": "FACEBOOK", "login_name": "josemarsilva", "login_password": "Secret@123", "cod_tipo_ambiente": "HML"}}, "status_code": 1, "status_message": "Success"}}
```



### 3.5. Guia para Implantação ###

* Obtenha o último pacote (.war) estável gerado disponível na sub-pasta './dist'.
* Configure os arquivos de parametrizações conforme o seu 
* Copie o pacote .war para o diretório ./webapp de seu servidor de aplicação Tomcat v8


### 3.6. Guia para Demonstração ###

* n/a


## Referências ##

* n/a
