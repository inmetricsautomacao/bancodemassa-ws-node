package br.com.inmetrics.bancodemassa.ws;

import static com.jayway.restassured.RestAssured.baseURI;
import static com.jayway.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Ignore;
import org.junit.Test;

import com.jayway.restassured.specification.ResponseSpecification;

public class WebServiceNodeTest {
	
	public WebServiceNodeTest() {
		baseURI = "http://10.82.248.107:8081/bancodemassa-ws-node";
	}

	@Test
	@Ignore
	public void verificarSeOWebServiceEstaNoAr() {
		ResponseSpecification resultado = given().when().then();
		assertEquals(200, resultado.get().statusCode());
	}
	
	@Test
	@Ignore
	public void deveTrazerUmaMassaParaOBancoBradesco() {
		final String json = getJsonModel("HML", "ETL", "ETL.LOGIN");
		String ecQueTemBancoBradesco = given().when().get("/dados/{json}", json).then().extract().asString();
		assertNotNull(ecQueTemBancoBradesco);
	}
	
	@Test
	@Ignore
	public void deveTrazerTodasAsClassesDeMassa() {
		final String tipoDeMassa = "ETL";
		String classes = given().when().get("/classesDeMassa/{tipoDeMassa}", tipoDeMassa).then().extract().asString();
		assertNotNull(classes);
	}
		
	private static String getJsonModel(String environmentType, String contentType, String classeMassa){
		StringBuilder json = new StringBuilder();
		json.append("{\"node\":{");
		json.append("\"environment_type\":\""+environmentType+"\"");
		json.append(",\"content_type\":\""+contentType+"\"");
		json.append("}");
		json.append(",\"get\":{\"classe_massa\":\""+classeMassa+"\"}");
		json.append(",\"controle\":{\"reuso\": 0}}");
		return json.toString();
	}
	
}
