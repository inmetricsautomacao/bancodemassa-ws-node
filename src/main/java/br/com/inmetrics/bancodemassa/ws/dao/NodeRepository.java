package br.com.inmetrics.bancodemassa.ws.dao;

import static br.com.inmetrics.bancodemassa.ws.utils.Constantes.FX_NODE_MASS_GET;
import static br.com.inmetrics.bancodemassa.ws.utils.Constantes.FX_NODE_MASS_POST;
import static br.com.inmetrics.bancodemassa.ws.utils.Constantes.FX_NODE_MASS_PUT;
import static java.lang.String.format;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import com.google.gson.JsonObject;

@Repository
public class NodeRepository implements DaoGenerico<JsonObject> {
	
	@Autowired JdbcTemplate jdbcTemplate;

	@Override
	public String buscar(JsonObject json) {
		String jsonResult = jdbcTemplate.queryForObject(format("select %s('%s')", FX_NODE_MASS_GET, json), String.class);
		return jsonResult;
	}

	@Override
	public String incluir(JsonObject json) {
		String jsonResult = jdbcTemplate.queryForObject(format("select %s('%s')", FX_NODE_MASS_POST, json), String.class);
		return jsonResult;
	}

	@Override
	public String alterarStatus(JsonObject json) {
		String jsonResult = jdbcTemplate.queryForObject(format("select %s('%s')", FX_NODE_MASS_PUT, json), String.class);
		return jsonResult;
	}

	@Override
	public List<String> buscarTodasAsClassesDeMassa(final String tipoDaMassa) {
		String query = "select * from bm_node_classe_massa where classe_massa like '%" + tipoDaMassa + "%'";
		List<String> resultado = jdbcTemplate.query(query, new ResultSetExtractor<List<String>>(){
			@Override
			public List<String> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<String> result = new ArrayList<String>();
				while (rs.next()) {
					result.add(rs.getString("classe_massa"));
				}
				return result;
			}
		});
		return resultado;
	}

}
