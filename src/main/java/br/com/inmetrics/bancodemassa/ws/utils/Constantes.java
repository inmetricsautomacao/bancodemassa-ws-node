package br.com.inmetrics.bancodemassa.ws.utils;

public final class Constantes {
	
	public static final String FX_NODE_MASS_GET = "fx_node_test_data_get";
	public static final String FX_NODE_MASS_POST = "fx_node_test_data_post";
	public static final String FX_NODE_MASS_PUT = "fx_node_test_data_put";
	
}
