package br.com.inmetrics.bancodemassa.ws.resource;

import static br.com.inmetrics.bancodemassa.ws.utils.Utils.parserStringTOJson;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.inmetrics.bancodemassa.ws.dao.NodeRepository;

@RestController
public class NodeService {
	
	@Autowired NodeRepository nodeRepository;
	
	@RequestMapping(value = "/dados/{json:.+}", method = RequestMethod.GET, headers = "Accept=application/json")
	public String buscar(@PathVariable String json) {
		return nodeRepository.buscar(parserStringTOJson(json));
	}
	
	@RequestMapping(value = "/incluirDados/json", method = RequestMethod.POST, headers = "Accept=application/json")
	public String incluir(@RequestBody String json) {
		return nodeRepository.incluir(parserStringTOJson(json));
	}
	
	@RequestMapping(value = "/atualizarStatus/json", method = RequestMethod.PUT, headers = "Accept=application/json")
	public String alterar(@RequestBody String json) {
		return nodeRepository.alterarStatus(parserStringTOJson(json));
	}
	
	@RequestMapping(value = "/classesDeMassa/{tipoDeMassa}", method = RequestMethod.GET)
	public List<String> buscarTodasAsClasses(@PathVariable String tipoDeMassa) {
		return nodeRepository.buscarTodasAsClassesDeMassa(tipoDeMassa);
	}
	
}