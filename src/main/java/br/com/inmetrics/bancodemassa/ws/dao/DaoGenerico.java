package br.com.inmetrics.bancodemassa.ws.dao;

import java.util.List;

public interface DaoGenerico<T> {
	
	public String buscar(T json);
	
	public String incluir(T json);
	
	public String alterarStatus(T json);
	
	public List<String> buscarTodasAsClassesDeMassa(String tipoDaMassa);
	
}
