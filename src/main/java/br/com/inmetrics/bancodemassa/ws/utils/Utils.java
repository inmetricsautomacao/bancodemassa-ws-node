package br.com.inmetrics.bancodemassa.ws.utils;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public final class Utils {
	
	public static JsonObject parserStringTOJson(String text){
		JsonParser parse = new JsonParser();
		JsonObject json = new JsonObject();
		json = parse.parse(text).getAsJsonObject();
		return json;
	}
	
}
